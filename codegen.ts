import type { CodegenConfig } from "@graphql-codegen/cli";

const DEFAULT_URL = "https://www.dnd5eapi.co/graphql";

const config: CodegenConfig = {
   schema: process.env.DND_SCHEMA_URL ?? DEFAULT_URL,
   generates: {
        "./src/types.ts": {
            plugins: [
                "typescript",
            ],
        }
   }
};

export default config;
