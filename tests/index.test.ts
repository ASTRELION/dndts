import * as dnd from "../src";
import type { dndTypes } from "../src";

jest.setTimeout(1000 * 60 * 2);

describe("Tests", () =>
{
    let consoleSpy: jest.SpyInstance;
    beforeEach(() =>
    {
        consoleSpy = jest.spyOn(console, "log");
    })

    afterEach(() =>
    {
        expect(consoleSpy).not.toHaveBeenCalled();
        consoleSpy.mockRestore();
    })

    test("Basics", async () =>
    {
        expect(dnd.getBaseURL()).toBe("https://www.dnd5eapi.co");
        dnd.setBaseURL("https://example.com");
        expect(dnd.getBaseURL()).toBe("https://example.com");
        dnd.resetBaseURL();
        expect(dnd.getBaseURL()).toBe("https://www.dnd5eapi.co");

        process.env.DND_API_URL = "https://example.com";
        dnd.resetBaseURL();
        expect(dnd.getBaseURL()).toBe("https://example.com");
        delete process.env.DND_API_URL;
        dnd.resetBaseURL();
        expect(dnd.getBaseURL()).toBe("https://www.dnd5eapi.co");

        expect((await dnd.query("/api/monsters", {challenge_rating: [1, 2]})).results).toHaveLength(68);
        expect(await dnd.query("/api/blah")).toMatchObject({
            error: true,
            statusCode: 404,
            statusText: "Not Found",
            requestedURL: "https://www.dnd5eapi.co/api/blah",
        });
    });

    test("Basic API", async () =>
    {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const endpoints: any[][] = [
            [dnd.getAbilityScores, dnd.getAbilityScore],
            [dnd.getAlignments, dnd.getAlignment],
            [dnd.getBackgrounds, dnd.getBackground],
            [dnd.getClasses, dnd.getClass],
            [dnd.getConditions, dnd.getCondition],
            [dnd.getDamageTypes, dnd.getDamageType],
            [dnd.getEquipments, dnd.getEquipment],
            [dnd.getEquipmentCategories, dnd.getEquipmentCategory],
            [dnd.getFeats, dnd.getFeat],
            [dnd.getFeatures, dnd.getFeature],
            [dnd.getLanguages, dnd.getLanguage],
            [dnd.getMagicItems, dnd.getMagicItem],
            [dnd.getMagicSchools, dnd.getMagicSchool],
            [dnd.getMonsters, dnd.getMonster],
            [dnd.getProficiencies, dnd.getProficiency],
            [dnd.getRaces, dnd.getRace],
            [dnd.getRuleSections, dnd.getRuleSection],
            [dnd.getRules, dnd.getRule],
            [dnd.getSkills, dnd.getSkill],
            [dnd.getSpells, dnd.getSpell],
            [dnd.getSubclasses, dnd.getSubclass],
            [dnd.getSubraces, dnd.getSubrace],
            [dnd.getTraits, dnd.getTrait],
            [dnd.getWeaponProperties, dnd.getWeaponProperty],
        ];

        const tests: Array<() => Promise<void>> = [];
        for (const pair of endpoints)
        {
            tests.push(async () =>
            {
                const items = await pair[0]();
                expect(items.length).toBeGreaterThan(0);
                for (const i of items)
                {
                    const item = await pair[1](i.index);
                    expect(item.index).toBe(i.index);
                    expect(item.name).toBe(i.name);
                }
            });
        }

        await Promise.all(tests.map((x) => x()));

        // test args
        expect(await dnd.getMonsters({challenge_rating: 1})).toHaveLength(25);
        expect(await dnd.getMonsters({challenge_rating: [1, 2]})).toHaveLength(68);
        expect(await dnd.getSpells({level: 1})).toHaveLength(49);
        expect(await dnd.getSpells({level: [1, 2]})).toHaveLength(103);
        expect(await dnd.getSpells({level: [1, 2], school: ["evocation", "illusion"]})).toHaveLength(30);

        // test getAll()
        let abilityScores = await dnd.getAll(await dnd.getAbilityScores());
        expect(abilityScores).toHaveLength(6);
        for (const score of abilityScores)
        {
            expect(score).toHaveProperty("full_name");
        }
        abilityScores = await dnd.getAll(dnd.getAbilityScores());
        expect(abilityScores).toHaveLength(6);
        for (const score of abilityScores)
        {
            expect(score).toHaveProperty("full_name");
        }
    });

    test("Extended API", async () =>
    {
        const classes = await dnd.getClasses();
        for (const clazz of classes)
        {
            expect((await dnd.getClassFeatures(clazz.index)).length).toBeGreaterThan(0);
            expect(await dnd.getClassLevel(clazz.index, 1)).toHaveProperty("level", 1);
            expect((await dnd.getClassLevelFeatures(clazz.index, 1)).length).toBeGreaterThan(0);
            expect((await dnd.getClassLevels(clazz.index)).length).toBeGreaterThan(0);
            expect((await dnd.getClassLevelSpells(clazz.index, 1)).length).toBeGreaterThanOrEqual(0);
            expect(await dnd.getClassSpellcasting(clazz.index));
            expect((await dnd.getClassSpells(clazz.index)).length).toBeGreaterThanOrEqual(0);
            expect(await dnd.getClassSubclasses(clazz.index)).toHaveLength(1);
            expect(await dnd.getClassMulticlassing(clazz.index));
            expect((await dnd.getClassProficiencies(clazz.index)).length).toBeGreaterThan(0);
        }

        const races = await dnd.getRaces();
        for (const race of races)
        {
            expect((await dnd.getRaceSubraces(race.index)).length).toBeGreaterThanOrEqual(0);
            expect((await dnd.getRaceProficiencies(race.index)).length).toBeGreaterThanOrEqual(0);
            expect((await dnd.getRaceTraits(race.index)).length).toBeGreaterThanOrEqual(0);
        }

        const subclasses = await dnd.getSubclasses();
        for (const subclass of subclasses)
        {
            expect((await dnd.getSubclassFeatures(subclass.index)).length).toBeGreaterThan(0);
            const levels = await dnd.getSubclassLevels(subclass.index);
            expect(levels.length).toBeGreaterThan(0);
            expect(await dnd.getSubclassLevel(subclass.index, levels[0].level));
            expect((await dnd.getSubclassLevelFeatures(subclass.index, levels[0].level)).length).toBeGreaterThan(0);
        }

        const subraces = await dnd.getSubraces();
        for (const subrace of subraces)
        {
            expect((await dnd.getSubraceProficiencies(subrace.index)).length).toBeGreaterThanOrEqual(0);
            expect((await dnd.getSubraceTraits(subrace.index)).length).toBeGreaterThan(0);
        }
    });

    test("Utility variables", () =>
    {
        for (const weapon of dnd.aquaticWeapons)
        {
            expect(weapon.length).toBeGreaterThan(0);
        }

        let last_cr = -1;
        for (const cr of dnd.challengeRatings)
        {
            expect(cr).toBeGreaterThanOrEqual(0);
            expect(cr).toBeLessThanOrEqual(30);
            expect(cr).toBeGreaterThan(last_cr);
            last_cr = cr;
        }

        let last_unit = 0;
        for (const unit of Object.keys(dnd.exchangeRates))
        {
            expect(dnd.exchangeRates[unit]).toBeGreaterThan(last_unit);
            last_unit = dnd.exchangeRates[unit];
        }

        let last_xp = -1;
        for (const xp of dnd.levelXP)
        {
            expect(xp).toBeGreaterThanOrEqual(0);
            expect(xp).toBeLessThanOrEqual(355000);
            expect(xp).toBeGreaterThanOrEqual(last_xp);
            last_xp = xp;
        }
    });

    test("Utility functions", async () =>
    {
        const shortsword = await dnd.getEquipment("shortsword") as dndTypes.Weapon;
        expect(dnd.isAquaticWeapon(shortsword)).toBe(true);
        const crossbow = await dnd.getEquipment("crossbow-heavy");
        expect(dnd.isAquaticWeapon(crossbow)).toBe(true);
        const longsword = await dnd.getEquipment("longsword");
        expect(dnd.isAquaticWeapon(longsword)).toBe(false);
        expect(dnd.isAquaticWeapon("dAgGeR")).toBe(true);
        expect(dnd.isAquaticWeapon("CLUB")).toBe(false);
        expect(dnd.isAquaticWeapon({ index: "tridEnt", 5: "blah" })).toBe(true);
        expect(dnd.isAquaticWeapon({ index: "lAnce", blah: "blah" })).toBe(false);

        expect(dnd.convertCurrency(2, dnd.Currency.sp, dnd.Currency.gp)).toBe(0.2);
        expect(dnd.convertCurrency(2, "sp", dnd.Currency.gp, true)).toBe(1);
        expect(dnd.convertCurrency(2, dnd.Currency.gp, "sp")).toBe(20);
        expect(dnd.convertCurrency(2, "gp", "sp", true)).toBe(20);
        expect(() => { dnd.convertCurrency(2, "blah", dnd.Currency.sp, true); }).toThrow();
        expect(() => { dnd.convertCurrency(2, dnd.Currency.gp, "blah", true); }).toThrow();

        expect(dnd.xpToLevel(-1)).toBeNull();
        expect(dnd.xpToLevel(0)).toBe(1);
        expect(dnd.xpToLevel(299)).toBe(1);
        expect(dnd.xpToLevel(300)).toBe(2);
        expect(dnd.xpToLevel(354999)).toBe(19);
        expect(dnd.xpToLevel(355000)).toBe(20);
        expect(dnd.xpToLevel(400000)).toBe(20);

        expect(dnd.abilityModifier(-100)).toBe(-5);
        expect(dnd.abilityModifier(1)).toBe(-5);
        expect(dnd.abilityModifier(2)).toBe(-4);
        expect(dnd.abilityModifier(9)).toBe(-1);
        expect(dnd.abilityModifier(10)).toBe(0);
        expect(dnd.abilityModifier(11)).toBe(0);
        expect(dnd.abilityModifier(12)).toBe(1);
        expect(dnd.abilityModifier(20)).toBe(5);
        expect(dnd.abilityModifier(30)).toBe(10);
        expect(dnd.abilityModifier(100)).toBe(10);
    });
});
