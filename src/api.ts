import * as dndTypes from "./types";

const DEFAULT_URL = "https://www.dnd5eapi.co";
let apiURL = process.env.DND_API_URL ?? DEFAULT_URL;

/**
 * Type returned from all enumerated endpoints. E.g. listing all available
 * ability scores. Use `getAll()` to get full data for `Enumerated[]`.
 */
export type Enumerated = {
    index: string,
    name: string,
    url: string
}

/**
 * Type returned from an invalid or failed API query.
 */
export type ErrorResponse = {
    error: true,
    statusCode: number,
    statusText: string,
    requestedURL: string
}

/**
 * Set the API base URL to use. By default this is https://www.dnd5eapi.co. This
 * should be the base URL of the API, do not include "/api/".
 * @param url the URL to use
 */
export function setBaseURL(url: string)
{
    apiURL = url;
}

/**
 * Get the base API URL, defaults to https://www.dnd5eapi.co.
 * @returns the base URL in use
 */
export function getBaseURL()
{
    return apiURL;
}

/**
 * Resets the API URL to https://www.dnd5eapi.co, or `DND_API_URL` if set.
 */
export function resetBaseURL()
{
    apiURL = process.env.DND_API_URL ?? DEFAULT_URL;
}

/**
 * Perform a general query against the D&D API.
 * @param endpoint the endpoint to query, .e.g `"/api/monsters"`
 * @param args a list of URL parameters to add, e.g. `{challenge_rating: [1,2]}`
 * @returns the JSON response, or an `ErrorResponse` on invalid requests
 */
export async function query(endpoint: string, args = {}): Promise<ErrorResponse | any>
{
    let url = new URL(endpoint, apiURL).href;
    if (Object.keys(args).length > 0)
    {
        const parameters: string[] = [];
        for (const [key, value] of Object.entries(args))
        {
            if (Array.isArray(value))
            {
                for (const v of value)
                {
                    parameters.push(`${key}=${v}`);
                }
            }
            else
            {
                parameters.push(`${key}=${value}`);
            }
        }
        url += `?${parameters.join("&")}`;
    }

    const response = fetch(url, { credentials: "include", cache: "default" });
    return response.then(result =>
    {
        if (!result.ok)
        {
            return {
                error: true,
                statusCode: result.status,
                statusText: result.statusText,
                requestedURL: url
            } as ErrorResponse;
        }

        return result.json();
    });
}

/**
 * Get each individual item from the result of an enumerated endpoint. This
 * queries the API for each item in `enumerated`, so it may take a while for
 * large arrays.
 *
 * Examples:
 * ```typescript
 * const monsters1 = await getAll(getMonsters()) as dndTypes.Monster[];
 * const monsters2 = await getAll(await getMonsters()) as dndTypes.Monster[];
 * ```
 * @param enumerated the enumerated array to get individual data for,
 * e.g. `await getMonsters()` or `getMonsters()`
 * @returns each item in `enumerated`'s full data
 */
export async function getAll(enumerated: Enumerated[] | Promise<Enumerated[]>)
{
    return Promise.all((await enumerated).map(item => query(item.url)));
}

//#region Extended API

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function getClassFeatures(index: string, args: {subclass?: string | string[], [key: string]: any} = {}): Promise<Enumerated[]>
{
    return query(`/api/classes/${index}/features`, args).then(result => result.results);
}

export async function getClassLevel(index: string, classLevel: number, args = {}): Promise<dndTypes.Level>
{
    return query(`/api/classes/${index}/levels/${classLevel}`, args);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function getClassLevelFeatures(index: string, classLevel: number, args: {subclass?: string | string[], [key: string]: any} = {}): Promise<Enumerated[]>
{
    return query(`/api/classes/${index}/levels/${classLevel}/features`, args).then(result => result.results);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function getClassLevels(index: string, args: {subclass?: string | string[], [key: string]: any} = {}): Promise<dndTypes.Level[]>
{
    return query(`/api/classes/${index}/levels`, args);
}

export async function getClassLevelSpells(index: string, spellLevel: number, args = {}): Promise<dndTypes.Spell[]>
{
    return query(`/api/classes/${index}/levels/${spellLevel}/spells`, args).then(result => result.results);
}

export async function getClassSpellcasting(index: string, args = {}): Promise<dndTypes.ClassSpellcasting>
{
    return query(`/api/classes/${index}/spellcasting`, args);
}

export async function getClassSpells(index: string, args = {}): Promise<Enumerated[]>
{
    return query(`/api/classes/${index}/spells`, args).then(result => result.results);
}

export async function getClassSubclasses(index: string, args = {}): Promise<Enumerated[]>
{
    return query(`/api/classes/${index}/subclasses`, args).then(result => result.results);
}

export async function getClassMulticlassing(index: string, args = {}): Promise<dndTypes.Multiclassing>
{
    return query(`/api/classes/${index}/multi-classing`, args);
}

export async function getClassProficiencies(index: string, args = {}): Promise<Enumerated[]>
{
    return query(`/api/classes/${index}/proficiencies`, args).then(result => result.results);
}

export async function getRaceSubraces(index: string, args = {}): Promise<Enumerated[]>
{
    return query(`/api/races/${index}/subraces`, args).then(result => result.results);
}

export async function getRaceProficiencies(index: string, args = {}): Promise<Enumerated[]>
{
    return query(`/api/races/${index}/proficiencies`, args).then(result => result.results);
}

export async function getRaceTraits(index: string, args = {}): Promise<Enumerated[]>
{
    return query(`/api/races/${index}/traits`, args).then(result => result.results);
}

export async function getSubclassFeatures(index: string, args = {}): Promise<Enumerated[]>
{
    return query(`/api/subclasses/${index}/features`, args).then(result => result.results);
}

export async function getSubclassLevel(index: string, subclassLevel: number, args = {}): Promise<dndTypes.Level>
{
    return query(`/api/subclasses/${index}/levels/${subclassLevel}`, args);
}

export async function getSubclassLevelFeatures(index: string, subclassLevel: number, args = {}): Promise<Enumerated[]>
{
    return query(`/api/subclasses/${index}/levels/${subclassLevel}/features`, args).then(result => result.results);
}

export async function getSubclassLevels(index: string, args = {}): Promise<dndTypes.Level[]>
{
    return query(`/api/subclasses/${index}/levels`, args);
}

export async function getSubraceProficiencies(index: string, args = {}): Promise<Enumerated[]>
{
    return query(`/api/subraces/${index}/proficiencies`, args).then(result => result.results);
}

export async function getSubraceTraits(index: string, args = {}): Promise<Enumerated[]>
{
    return query(`/api/subraces/${index}/traits`, args).then(result => result.results);
}

//#endregion

//#region Basic API

export async function getAbilityScore(index: string, args = {}): Promise<dndTypes.AbilityScore>
{
    return query(`/api/ability-scores/${index}`, args);
}

export async function getAbilityScores(args = {}): Promise<Enumerated[]>
{
    return query("/api/ability-scores", args).then(result => result.results);
}

export async function getAlignment(index: string, args = {}): Promise<dndTypes.Alignment>
{
    return query(`/api/alignments/${index}`, args);
}

export async function getAlignments(args = {}): Promise<Enumerated[]>
{
    return query("/api/alignments", args).then(result => result.results);
}

export async function getBackground(index: string, args = {}): Promise<dndTypes.Background>
{
    return query(`/api/backgrounds/${index}`, args);
}

export async function getBackgrounds(args = {}): Promise<Enumerated[]>
{
    return query("/api/backgrounds", args).then(result => result.results);
}

export async function getClass(index: string, args = {}): Promise<dndTypes.Class>
{
    return query(`/api/classes/${index}`, args);
}

export async function getClasses(args = {}): Promise<Enumerated[]>
{
    return query("/api/classes", args).then(result => result.results);
}

export async function getCondition(index: string, args = {}): Promise<dndTypes.Condition>
{
    return query(`/api/conditions/${index}`, args);
}

export async function getConditions(args = {}): Promise<Enumerated[]>
{
    return query("/api/conditions", args).then(result => result.results);
}

export async function getDamageType(index: string, args = {}): Promise<dndTypes.DamageType>
{
    return query(`/api/damage-types/${index}`, args);
}

export async function getDamageTypes(args = {}): Promise<Enumerated[]>
{
    return query("/api/damage-types", args).then(result => result.results);
}

/**
 * You will have to manually cast the result of this function to access
 * specific equipment properties. E.g. `getEquipment("club") as dndTypes.Weapon`.
 */
export async function getEquipment(index: string, args = {}): Promise<dndTypes.IEquipment>
{
    return query(`/api/equipment/${index}`, args);
}

export async function getEquipments(args = {}): Promise<Enumerated[]>
{
    return query("/api/equipment", args).then(result => result.results);
}

export async function getEquipmentCategory(index: string, args = {}): Promise<dndTypes.EquipmentCategory>
{
    return query(`/api/equipment-categories/${index}`, args);
}

export async function getEquipmentCategories(args = {}): Promise<Enumerated[]>
{
    return query("/api/equipment-categories", args).then(result => result.results);
}

export async function getFeat(index: string, args = {}): Promise<dndTypes.Feat>
{
    return query(`/api/feats/${index}`, args);
}

export async function getFeats(args = {}): Promise<Enumerated[]>
{
    return query("/api/feats", args).then(result => result.results);
}

export async function getFeature(index: string, args = {}): Promise<dndTypes.Feature>
{
    return query(`/api/features/${index}`, args);
}

export async function getFeatures(args = {}): Promise<Enumerated[]>
{
    return query("/api/features", args).then(result => result.results);
}

export async function getLanguage(index: string, args = {}): Promise<dndTypes.Language>
{
    return query(`/api/languages/${index}`, args);
}

export async function getLanguages(args = {}): Promise<Enumerated[]>
{
    return query("/api/languages", args).then(result => result.results);
}

export async function getMagicItem(index: string, args = {}): Promise<dndTypes.MagicItem>
{
    return query(`/api/magic-items/${index}`, args);
}

export async function getMagicItems(args = {}): Promise<Enumerated[]>
{
    return query("/api/magic-items", args).then(result => result.results);
}

export async function getMagicSchool(index: string, args = {}): Promise<dndTypes.MagicSchool>
{
    return query(`/api/magic-schools/${index}`, args);
}

export async function getMagicSchools(args = {}): Promise<Enumerated[]>
{
    return query("/api/magic-schools", args).then(result => result.results);
}

export async function getMonster(index: string, args = {}): Promise<dndTypes.Monster>
{
    return query(`/api/monsters/${index}`, args);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function getMonsters(args: {challenge_rating?: number | number[], [key: string]: any} = {}): Promise<Enumerated[]>
{
    return query("/api/monsters", args).then(result => result.results);
}

export async function getProficiency(index: string, args = {}): Promise<dndTypes.Proficiency>
{
    return query(`/api/proficiencies/${index}`, args);
}

export async function getProficiencies(args = {}): Promise<Enumerated[]>
{
    return query("/api/proficiencies", args).then(result => result.results);
}

export async function getRace(index: string, args = {}): Promise<dndTypes.Race>
{
    return query(`/api/races/${index}`, args);
}

export async function getRaces(args = {}): Promise<Enumerated[]>
{
    return query("/api/races", args).then(result => result.results);
}

export async function getRuleSection(index: string, args = {}): Promise<dndTypes.RuleSection>
{
    return query(`/api/rule-sections/${index}`, args);
}

export async function getRuleSections(args = {}): Promise<Enumerated[]>
{
    return query("/api/rule-sections", args).then(result => result.results);
}

export async function getRule(index: string, args = {}): Promise<dndTypes.Rule>
{
    return query(`/api/rules/${index}`, args);
}

export async function getRules(args = {}): Promise<Enumerated[]>
{
    return query("/api/rules", args).then(result => result.results);
}

export async function getSkill(index: string, args = {}): Promise<dndTypes.Skill>
{
    return query(`/api/skills/${index}`, args);
}

export async function getSkills(args = {}): Promise<Enumerated[]>
{
    return query("/api/skills", args).then(result => result.results);
}

export async function getSpell(index: string, args = {}): Promise<dndTypes.Spell>
{
    return query(`/api/spells/${index}`, args);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function getSpells(args: {level?: number | number[], school?: string | string[], [key: string]: any} = {}): Promise<Enumerated[]>
{
    return query("/api/spells", args).then(result => result.results);
}

export async function getSubclass(index: string, args = {}): Promise<dndTypes.Subclass>
{
    return query(`/api/subclasses/${index}`, args);
}

export async function getSubclasses(args = {}): Promise<Enumerated[]>
{
    return query("/api/subclasses", args).then(result => result.results);
}

export async function getSubrace(index: string, args = {}): Promise<dndTypes.Subrace>
{
    return query(`/api/subraces/${index}`, args);
}

export async function getSubraces(args = {}): Promise<Enumerated[]>
{
    return query("/api/subraces", args).then(result => result.results);
}

export async function getTrait(index: string, args = {}): Promise<dndTypes.Trait>
{
    return query(`/api/traits/${index}`, args);
}

export async function getTraits(args = {}): Promise<Enumerated[]>
{
    return query("/api/traits", args).then(result => result.results);
}

export async function getWeaponProperty(index: string, args = {}): Promise<dndTypes.WeaponProperty>
{
    return query(`/api/weapon-properties/${index}`, args);
}

export async function getWeaponProperties(args = {}): Promise<Enumerated[]>
{
    return query("/api/weapon-properties", args).then(result => result.results);
}

//#endregion
