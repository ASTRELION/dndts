/**
 * Array of weapon types that can be used in water with no penalty.
 */
export const aquaticWeapons = [
    "crossbow",
    "dagger",
    "dart",
    "javelin",
    "net",
    "shortsword",
    "spear",
    "trident"
] as const;

/**
 * Possible monster Challenge Ratings.
 */
export const challengeRatings = [
    0, 0.125, 0.25, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
    14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30
] as const;

/**
 * Units of currency.
 */
export enum Currency
{
    /** Copper */
    cp = "cp",
    /** Silver */
    sp = "sp",
    /** Electrum */
    ep = "ep",
    /** Gold */
    gp = "gp",
    /** Platinum */
    pp = "pp"
};

/**
 * Exchange rates for each unit of currency in terms of Copper (cp).
 */
export const exchangeRates = {
    /** 1 Copper */
    [Currency.cp]: 1,
    /** 1 Silver = 10 cp */
    [Currency.sp]: 10,
    /** 1 Electrum = 50 cp */
    [Currency.ep]: 50,
    /** 1 Gold = 100 cp */
    [Currency.gp]: 100,
    /** 1 Platinum = 1000 cp */
    [Currency.pp]: 1000
} as const;

/**
 * Experience requirements for each character level (0-20).
 *
 * E.g.,
 * `LEVEL_XP[1] == 0`,
 * `LEVEL_XP[2] == 300`,
 * ...,
 * `LEVEL_XP[20] == 355000`
 */
export const levelXP = [
    0, 0,   300,    900,    2700,   // 0-4   (prof 2)
    6500,   14000,  23000,  34000,  // 5-8   (prof 3)
    48000,  64000,  85000,  100000, // 9-12  (prof 4)
    120000, 140000, 165000, 195000, // 13-16 (prof 5)
    225000, 265000, 305000, 355000  // 17-20 (prof 6)
] as const;

/**
 * Determine if the given weapon can be used in water without disadvantage.
 *
 * Examples:
 * ```typescript
 * isAquaticWeapon(await getEquipment("dagger")); // true
 * isAquaticWeapon("dagger"); // true
 * isAquaticWeapon({ index: "dagger" }); // true
 * isAquaticWeapon(await getEquipment("club")); // false
 * ```
 * @param weapon the weapon to check
 * @returns `true` if the weapon is aquatic, `false` otherwise
 */
export function isAquaticWeapon(weapon: { index: string, [key: string]: any } | string)
{
    if (typeof weapon === "string")
    {
        return aquaticWeapons.some(a => weapon.toLowerCase().includes(a));
    }

    return aquaticWeapons.some(a => weapon.index.toLowerCase().includes(a));
}

/**
 * Convert an amount of currency to a different unit of currency.
 *
 * Examples:
 * ```typescript
 * convertCurrency(2, "sp", "gp"); // 0.2
 * convertCurrency(2, "sp", "gp", true); // 1
 * convertCurrency(2, "gp", "sp"); // 20
 * ```
 * @param quantity quantity of `fromUnit` to convert
 * @param fromUnit unit to convert *from*
 * @param toUnit unit to convert *to*
 * @param round `true` rounds the result up to a whole integer, returning a
 * float otherwise (default)
 * @returns the amount of `toUnit` currency converted
 */
export function convertCurrency(quantity: number, fromUnit: Currency | string, toUnit: Currency | string, round = false)
{
    fromUnit = fromUnit.toLowerCase();
    toUnit = toUnit.toLowerCase();

    if (!Object.values(Currency).includes(fromUnit as Currency)
        || !Object.values(Currency).includes(toUnit as Currency))
    {
        throw new Error(`Units must be one of ${Object.values(Currency).join(", ")}`);
    }

    let exchanged = (exchangeRates[fromUnit] * quantity) / exchangeRates[toUnit];
    if (round) exchanged = Math.ceil(exchanged);
    return exchanged;
}

/**
 * Convert the given xp amount to the corresponding character level.
 *
 * Examples:
 * ```typescript
 * xpToLevel(0); // 1
 * xpToLevel(299); // 1
 * xpToLevel(300); // 2
 * ```
 * @param xp experience points to convert
 * @returns character level corresponding to `xp` amount, or `null` if no valid
 * level.
 */
export function xpToLevel(xp: number)
{
    for (let i = levelXP.length - 1; i >= 0; i--)
    {
        if (xp >= levelXP[i])
        {
            return i;
        }
    }

    return null;
}

/**
 * Determine the Ability Modifier from the given Ability Score. Minimum of -5
 * for a score of 1 and a maximum of 10 for a score of 30.
 *
 * Examples:
 * ```typescript
 * abilityModifier(10); // 0
 * abilityModifier(9); // -1
 * abilityModifier(12); // 1
 * ```
 * @param abilityScore the Ability Score to get the modifier for
 * @returns the Ability Modifier
 */
export function abilityModifier(abilityScore: number)
{
    return Math.max(-5, Math.min(10, Math.floor((abilityScore - 10) / 2)));
}
