# DnD.ts

[![Latest Release](https://gitlab.com/ASTRELION/dndts/-/badges/release.svg)](https://gitlab.com/ASTRELION/dndts/-/releases)
[![pipeline status](https://gitlab.com/ASTRELION/dndts/badges/main/pipeline.svg)](https://gitlab.com/ASTRELION/dndts/-/commits/main)
[![coverage report](https://gitlab.com/ASTRELION/dndts/badges/main/coverage.svg)](https://gitlab.com/ASTRELION/dndts/-/commits/main)

TypeScript API client for the [D&D 5e SRD API][api], which sources material from the [D&D 5e SRD (OGL v1.0a)][ogl]. This package also supports using a custom endpoint for both type generation and making API requests.

This client [generates types](./src/types.ts) from the API's GraphQL schema and [offers functions](./src/api.ts) to retrieve typed data from each endpoint. Some utility constants and functions are also added to fill some gaps in the API's offerings. All API queries are provided as `async` functions and query the standard JSON endpoints, *not* the GraphQL endpoint. All API functions are prefixed with `get`.

## Installation & Usage

**Install**

```bash
echo @astrelion:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
yarn add @astrelion/dndts
```

**Import**

```typescript
// for API query functions
import * as dnd from "@astrelion/dndts";
// for TypeScript typings
import type { dndTypes } from "@astrelion/dndts";
```

**Invoke**

```typescript
// async/await
const wizard: dndTypes.Class = await dnd.getClass("wizard");
console.log(wizard);
// promise/then
dnd.getRace("elf").then((race) =>
{
    console.log(race);
});
```

*See the [documentation][docs] for all functions, variables, and types.*

### Using an Alternate API/Schema

If you're hosting the [D&D API][api] yourself or are using an alternative host, you may want to change the endpoint this package uses.

If you want to use a custom API URL for making API requests, you may set the `DND_API_URL` environment variable or call `dnd.setBaseURL()` *before* making a request. The default URL used will be `DND_API_URL` if provided, otherwise it will be `"https://www.dnd5eapi.co"`.

If you're building this package yourself and want to generate types based on your hosted API, you may set the `DND_SCHEMA_URL` environment variable. This changes the URL for [`codegen.ts`](./codegen.ts) and subsequently `yarn run codegen`. If not provided, this defaults to `"https://www.dnd5eapi.co/graphql"`.

## [Documentation][docs]

## [License](./LICENSE)

```
DnD.ts
Copyright (C) 2023  ASTRELION

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

## Donate

[Liberapay][liberapay]

<a href="https://liberapay.com/ASTRELION/"><img src="https://img.shields.io/liberapay/patrons/ASTRELION.svg?logo=liberapay"></a>



[api]: https://www.dnd5eapi.co "D&D 5e SRD API"
[docs]: https://astrelion.gitlab.io/dndts/ "Documentation"
[liberapay]: https://liberapay.com/ASTRELION/ "ASTRELION's Liberapay"
[ogl]: https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf "D&D 5e SRD, OGL v1.0a"
